#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from flask import render_template
from flask import request
from flask_wtf import CsrfProtect

import forms # importamos nuestro modulo de formularios

app = Flask(__name__)
app.secret_key = 'secret_key'
csrf = CsrfProtect(app)

@app.route('/', methods=["GET","POST"])
def index():
    comment_form = forms.CommentForm(request.form) # creo una nueva instancia
    if request.method=="POST" and comment_form.validate():
        print comment_form.username.data
        print comment_form.email.data
        print comment_form.comment.data
    else:
        print "Error en el formulario"

    title = 'Formularios'
    return render_template('index.html', title = title , form = comment_form)

@app.route('/login', methods=["GET", "POST"])
def login():
    login_form = forms.Login_Form()
    title = 'Login'
    return render_template('login.html', title = title, form = login_form)



if __name__== '__main__':
    app.run(debug=True, port=5000)
