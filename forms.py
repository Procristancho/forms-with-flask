
from wtforms import Form
from wtforms import StringField, TextField
from wtforms.fields.html5 import EmailField
from wtforms import HiddenField
from wtforms import PasswordField

from wtforms import validators

def length_honeypot(form, field):
    if len(field.data) > 0:
        raise validators.ValidationError('El campo debe estar vacio.')

class CommentForm(Form):
    username = StringField('Usuario',
                            [
                              validators.Required(message='El username es requerido.!!'),
                              validators.length(min=4, max=25, message = 'Inrese un username valido.!!')
                            ]

                          )
    email = EmailField('Correo electronico',
                        [
                            validators.Required(message='Recuerda ingresar tu email'),
                            validators.Email(message='Ingresa un email valido.!!')
                        ]
                      )
    comment = TextField('Comentario')
    honeypot = HiddenField('',[length_honeypot])

class Login_Form(Form):
    username = StringField('Username',
                            [
                                validators.Required(message='El Usuario es requerido.!!'),
                                validators.length(min=4, max=25, message='Ingresa un Username valido')
                            ]
                          )
    password = PasswordField('Password',
                                [
                                    validators.Required(message='El password es requerido.!!')
                                ]
                            )
